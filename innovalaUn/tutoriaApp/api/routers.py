from rest_framework.routers import DefaultRouter
from tutoriaApp.api.views.tutor_viewsets import TutorViewSet
from tutoriaApp.api.views.tutoria_viewsets import TutoriaViewSet

router = DefaultRouter()

router.register(r'tutoria',TutoriaViewSet,basename = 'tutoria')
router.register(r'tutor',TutorViewSet,basename = 'tutor')

urlpatterns = router.urls