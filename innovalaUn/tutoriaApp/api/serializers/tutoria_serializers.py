from rest_framework import serializers

from tutoriaApp.models import Tutoria

class TutoriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tutoria
        exclude = ('state','created_date','modified_date','deleted_date')
    
    def to_representation(self,instance):
        return {
            'id': instance.id,
            'tema': instance.tema,
            'description': instance.description,
            'tutor': instance.tutor.apellidosTutor if instance.tutor is not None else '',
            'username': instance.user.description if instance.username is not None else ''
        }
