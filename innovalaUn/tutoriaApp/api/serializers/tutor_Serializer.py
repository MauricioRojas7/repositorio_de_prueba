from tutoriaApp.models import Tutor

from rest_framework import serializers

class TutorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tutor
        exclude = ('state','created_date','modified_date','deleted_date')
