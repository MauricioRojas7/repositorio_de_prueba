from django.db import models
from simple_history.models import HistoricalRecords
from usersApp.models import User
from baseApp.models import BaseModel

class Tutor(BaseModel):
    """Model definition for CategoryProduct."""

    # TODO: Define fields here
    temaTutor = models.TextField('Tema Tutor',blank = False,null = False)
    nombresTutor = models.CharField('Nombres tutor', max_length = 50,)
    apellidosTutor = models.CharField('Apellidos tutor', max_length = 50)
    emailTutor = models.EmailField('Email tutor', max_length = 100, unique=True)
    profesionTutor = models.CharField('Profesión tutor', max_length = 50)
    experienciaTutor = models.CharField('Experienciatutor', max_length = 50)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Tutor."""

        verbose_name = 'Tutor'
        verbose_name_plural = 'Tutores'

    def __str__(self):
        """Unicode representation of Tutor."""
        return self.nombresTutor

class Tutoria(BaseModel):
    """Model definition for Product."""

    # TODO: Define fields here
    name = models.CharField('Nombre Tutoria', max_length=150, unique = True,blank = False,null = False)
    description = models.TextField('Descripción Tutoria',blank = False,null = False)
    tutor = models.ForeignKey(Tutor, on_delete=models.CASCADE,verbose_name = 'Tutor', null = True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name = 'Usuario', null = True)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Tutoria."""

        verbose_name = 'Tutoria'
        verbose_name_plural = 'Tutorias'

    def __str__(self):
        """Unicode representation of Tutoria."""
        return self.name
