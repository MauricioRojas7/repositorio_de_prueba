from django.apps import AppConfig


class TutoriaappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tutoriaApp'
